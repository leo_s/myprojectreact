'use strict';

const PORT = 3000;

const Koa = require('koa');
const serve = require('koa-static');
const webpack = require('webpack');
//const { sequelize } = require('./models');
const models = require('./models');
const koaBody = require('koa-bodyparser');
const session = require('koa-generic-session');
const send = require('koa-send');
const SequelizeSessionStore = require('koa-generic-session-sequelize');
const passport = require('./middlewares/passport');
const router = require('./routers');

const port = process.env.PORT || PORT;
const app = new Koa();

const compiler = webpack(require('../webpack.config.js'), (err, stats) => {
  if (err || stats.hasErrors()) {
    /* eslint-disable no-console */
    console.log('There are webpack exception', err, stats.toJson('minimal'));
    /* eslint-enable no-console */
    return;
  }

  /* eslint-disable no-console */
  console.log('webpack initialized successfully');
  /* eslint-enable no-console */
});

compiler.watch({}, () => {
  /* eslint-disable no-console */
  console.log('building...');
  /* eslint-enable no-console */
});

app.use(serve('public'));

// body parser
app.use(koaBody());
app.keys = ['secret'];

const runServer = async function () {
    //rem for testing
    //await models.sequelize.sync({ force: true });
    await models.sequelize.sync({ force: true });

    // session middleware allows using ctx.session to set or get the sessions
    app.use(session({
        store: new SequelizeSessionStore(
            models.sequelize, {
                tableName: 'sessions',
            },
        )
    }));
    app.context.models = models;
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(router.routes());
    app.use(async function(ctx){
        await send(ctx, '/public/index.html')
    });
    //app.use(router.allowedMethods());

    return app.listen(port, () => {
        /* eslint-disable no-console */
        console.log(`Server is started on ${port} port`);
        /* eslint-enable no-console */
    });
};

const server = runServer();

module.exports = server;
