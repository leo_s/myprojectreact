module.exports = {
  development: {
    db: {
      name: 'my_project',
      username: 'mp_admin',
      password: '12345678',
      host: 'localhost',
      port: 3306,
      dialect: 'mysql',
      logging: true
    }
  },
  test: {
    db: {
      name: 'mp_test',
      username: 'mpt_admin',
      password: '12345678',
      host: 'localhost',
      port: 3306,
      dialect: 'mysql',
      logging: false
    }
  }
};