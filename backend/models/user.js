const Sequelize = require('sequelize');
//const bcrypt = require("bcrypt");

module.exports = (sequelize) => {
    const User = sequelize.define('User', {
        name: {
            type: Sequelize.STRING(50),
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        surname: {
            type: Sequelize.STRING(50),
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        email: {
            type: Sequelize.STRING(50),
            allowNull: false,
            unique : true,
            validate: {
                isEmail: true,
                notEmpty: true
            }
        },
        password: {
            type: Sequelize.STRING(70),
            allowNull: false,
            validate: {
                notEmpty: true
            }
        }
    }, {
        underscored: true,
        tableName: 'users',
        charset: 'utf8',
        collate: 'utf8_unicode_ci',
    });



    return User;
};