//const passport = require('../middlewares/passport');
const UtilService = require('../../services/util.service');

const createProfile = async (ctx) => {
    try {
        let {name, surname, email, password} = ctx.request.body;
        if(/(?=^.{6,10}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s).*$/.test(password) === false){
            ctx.throw(400, 'Password password should be at 6-10 characters, contain at least one number, one lowercase letter, one uppercase letter, one special character')
        }
        const encryptedPassword = await UtilService.hashPassword(password);
        await ctx.models.User.create({
            name,
            surname,
            email,
            password: encryptedPassword
        }).catch(
            error => {
                if (error.name.localeCompare('SequelizeValidationError') === 0 ||
                    error.name.localeCompare('SequelizeUniqueConstraintError') === 0){
                    //ctx.throw(400, error.errors["0"].message)
                    ctx.throw(400, error.message)
                } else {
                    throw error;
                }
            }
        );
        ctx.body = 'Account is created!';
    }
    catch (err) {
        ctx.throw(500, err);
    }

}

const showProfile = (ctx) => {
    if (ctx.isUnauthenticated()) {
        ctx.throw(401, 'Unauthenticated');
    }
    ctx.status = 200;
    //data from passport.deserializeUser
    ctx.body = ctx.state.user;
};

module.exports = {
    showProfile,
    createProfile,
};