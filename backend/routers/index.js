const Router = require('koa-router');
const {
    login,
    logout,
} = require('./auth');

const {
    showProfile,
    createProfile
} = require('./profile');

const {
    usersList,
    usersListFilter,
} = require('./users');

const router = new Router();

router.post('/createProfile', createProfile);
router.get('/showProfile', showProfile);
router.post('/login', login);
router.get('/logout', logout);
router.get('/usersList', usersList);
router.get('/usersList/filter/:person', usersListFilter);

module.exports = router;