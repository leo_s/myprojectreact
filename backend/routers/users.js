const usersList = async (ctx) => {
    try {
        if (ctx.isUnauthenticated()) {
            ctx.throw(401, 'Unauthenticated');
        }
        ctx.status = 200;
        const out = await ctx.models.User.findAll({
            attributes: ['id', 'name', 'surname']
        });
        ctx.body = out;
    }
    catch (err) {
        ctx.throw(500, err)
    }

};

const usersListFilter = async (ctx) => {
    try {
        if (ctx.isUnauthenticated()) {
            ctx.throw(401, 'Unauthenticated');
        }
        ctx.status = 200;
        const person = ctx.params.person.split(/\s+/);
        //const out1;
        if (person.length == 1) {//name or surname only.
            const out = await ctx.models.User.findAndCountAll({
                where: {
                    name: person[0],
                },
                attributes: ['id', 'name', 'surname']
            });
            if (out.count > 0) {
                ctx.body = out;
            }
            else {
                ctx.body = await ctx.models.User.findAndCountAll({
                    where: {
                        surname: person[0],
                    },
                    attributes: ['id', 'name', 'surname']
                });
            }
        }
        else {
            const out = await ctx.models.User.findAndCountAll({
                where: {//name is first surname is second
                    name: person[0],
                    surname: person[1],
                },
                attributes: ['id', 'name', 'surname']
            });
            if (out.count > 0) {
                ctx.body = out;
            }
            else {
                ctx.body = await ctx.models.User.findAndCountAll({
                    where: {//surname is first name is second
                        name: person[1],
                        surname: person[0],
                    },
                    attributes: ['id', 'name', 'surname']
                });
            }
        }
    }
    catch (err) {
        ctx.throw(500, err)
    }

};

module.exports = {
    usersList,
    usersListFilter,
};