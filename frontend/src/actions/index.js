import axios from 'axios';
import {FETCH_USER, TEST} from './types';
import history from '../utils/history'

export const fetchUser = () => async dispatch => {
    const res = await axios.get('/api/current_user');

    dispatch({type: FETCH_USER, payload: res.data});
};

export const createProfile = values => async dispatch => {
    const res = await axios.post('/api/surveys', values);

    dispatch({ type: FETCH_USER, payload: res.data });
};

export const submitSurvey = values => async dispatch => {
    const res = await axios.post('/api/surveys', values);

    dispatch({ type: FETCH_USER, payload: res.data });
};

export function addNumber(number = 15) {
    return{
        type: 'add_number',
        payload: number
    }
}

export function createProfileAction (values, callback) {
    const request = axios
        .post(`/createProfile`, values)
        .then(() => callback());

    return {
        type: TEST,
        payload: request
    };
}