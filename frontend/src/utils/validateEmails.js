const pattern = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

export default email => {

    if (pattern.test(email) === false) {
        return `These e-mail - "${email}" are invalid`;
    }
    return;
};