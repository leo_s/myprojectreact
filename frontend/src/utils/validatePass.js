const pattern = /(?=^.{6,10}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s).*$/;

const compare = (repPass, pass) => {
    if (pass.localeCompare(repPass) !== 0) {
        return `Passwords do not match`;
    }
    return;
};

const complexity = (pass) => {
    if(pattern.test(pass) === false){
        return `Password password should be at 6-10 characters, contain at least one number, one lowercase letter, one uppercase letter, one special character`;
    }
    return;
};

module.exports = {
    compare,
    complexity
};
