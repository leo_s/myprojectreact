import React, {Component} from 'react';
import {BrowserRouter, Route, Router} from 'react-router-dom';
import {connect} from 'react-redux';
import * as actions from '../actions';
import history from '../utils/history'

import '../styles/components/App.css';

import Header from './Header';
import Content from './Content';
import Main from './Main';
import Signup from './signup/Signup'

class App extends Component {
    render() {
        return (
            <Router history={history}>
                <BrowserRouter>
                    <div className='container'>
                        <div>
                            {/*<Signup/>*/}
                            <Header/>
                            <Route exact path="/" component={Content}/>
                            <Route path="/signup" component={props => <Signup {...props} />}/>
                            <Route path="/main" component={Main}/>

                        </div>
                    </div>
                </BrowserRouter>
            </Router>
        );
    }
}

export default connect(null, actions)(App);
//export default App;