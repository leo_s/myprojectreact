import _ from 'lodash';
import React, {Component} from 'react';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import SignupField from './SignupField';
import validateEmails from '../../utils/validateEmails';
import validatePass from '../../utils/validatePass';
import { createProfileAction } from '../../actions';


const FIELDS = [
    {label: 'Enter your name', name: 'name', noValueError: 'Provide you name', type: 'text'},
    {label: 'Enter your surname', name: 'surname', noValueError: 'Provide you surname', type: 'text'},
    {label: 'Enter your e-mail', name: 'email', noValueError: 'Provide an e-mail', type: 'text'},
    {label: 'Enter password', name: 'password', noValueError: 'Provide password', type: 'password'},
    {label: 'Repeat password', name: 'repPassword', noValueError: 'Repeat password', type: 'password'},
];

class Signup extends Component {
    renderFields() {
        return _.map(FIELDS, ({label, name, type}) => {
            return (
                <Field
                    key={name}
                    component={SignupField}
                    type={type} label={label}
                    name={name}
                />
            );
        });
    }

    onSubmit(values) {
        console.log('inside onSubmit');
        this.props.createProfileAction(values, () => {
            this.props.history.push("/main");
        });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.props.handleSubmit(this.onSubmit.bind(this))}>
                    {this.renderFields()}
                    <Link to="/" className="red btn-flat white-text">
                        Cansel
                    </Link>
                    <button type="submit" className="teal btn-flat right white-text">
                        Submit
                        <i className="material-icons right">done</i>
                    </button>
                </form>
                <div>
                </div>
            </div>
        );
    }
}

function validate(values) {
    const errors = {};

    errors.email = validateEmails(values.email || '');
    errors.password = validatePass.complexity(values.password || '');
    errors.repPassword = validatePass.compare((values.repPassword || ''), (values.password || ''));

    _.each(FIELDS, ({name, noValueError}) => {
        if (!values[name]) {
            errors[name] = noValueError;
        }
    });

    return errors;
}

function mapStateToProps(state) {
    // console.log('state', state);
    return {
        counter: state.auth.counter
    }
}

export default connect(
    mapStateToProps,
    { createProfileAction },
)(reduxForm({
    validate,
    form: 'signup'
})(Signup));