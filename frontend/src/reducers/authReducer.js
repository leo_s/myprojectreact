import { FETCH_USER, TEST } from '../actions/types';
const initStat = {
    counter: 37
}
export default function(state = initStat, action) {
    switch (action.type) {
        case FETCH_USER:
            return action.payload || false;
        case TEST:
            return {
                counter: state.counter + 100
            };
        default:
            return state;
    }
}
